#!/bin/sh
#### Defino los parametros de conexión a la BD mysql
sql_host="localhost"
slq_usuario="root"
sql_password="anibal123"
sql_db="cumplimiento"
sql_table="transaccion"
sql_file="transaccion.csv"

### Se monta los parámetros de conexión
sql_args="-h $sql_host -u $slq_usuario -p$sql_password -D $sql_db"

### Mi sentencia Sql para que la muestre
mysql $sql_args << END_SCRIPT
LOAD DATA INFILE '/home/anibal/Escritorio/cumplimiento/$sql_file' INTO TABLE $sql_db.$sql_table FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n';
END_SCRIPT
