!/bin/bash
#Script to Backup MySQL Database
#Author: Juan de Yzaguirre - Oct/2014
 
#MySQL settings
mysql_user=root	
mysql_password=anibal123
 
#Set Database to Backup
database=bienestar
 
#Set Backup Directory
backup_dir=/home/anibal/Documentos/mysql_1/
 
#Set logger
logfile=/home/anibal/Documentos/mysql_1/
 
#Check MySQL credentials
echo "Checking MySQL connection..."
mysql -u$mysql_user -p$mysql_password -e exit 2>/dev/null
dbstatus=`echo $?`
if [ $dbstatus -ne 0 ]; then
   echo "MySQL $mysql_user password incorrect"
   exit
else
   echo "MySQL $mysql_user password correct"
   echo "Succes! MySQL database connection established"
fi
 
#Dump and compress database backup
mysqldump -u$mysql_user -p$mysql_password $database > "$backup_dir"bd_bienestar$(date +%d-%m-%Y).sql
backup=`echo $?`
if [ "$backup" -ne 0 ]; then
   echo "[MYSQL ERROR]["$(date +%d-%m-%Y/%T)"] An error has occurred during the backup operation"
   echo "An error has occurred during the backup operation"
   exit
else
   echo "[MYSQL INFO]["$(date +%d-%m-%Y/%T)"] MySQL backup completed successfully" >> $logfile
   echo "MySQL backup completed successfully"
fi
 
#Copy Database to secondary HDD
scp "$backup_dir"bd_bienestar$(date +%d-%m-%Y).sql /home/anibal/Documentos/mysql_2/
 
#Delete files older than 15 days
find  /home/anibal/Documentos/mysql_1/* -mtime +2 -exec rm {} \;