#! /bin/bash
mkdir /home/Maritza.Az
mkdir /home/Maritza.Az/Maritza.Az
chmod 755 /home/Maritza.Az
chmod 755 /home/Maritza.Az/Maritza.Az
groupadd Maritza.Az
useradd -g Maritza.Az -s /bin/fake -d /home/Maritza.Az/Maritza.Az Maritza.Az
mkdir /home/Maritza.Az/Maritza.Az/files
chown Maritza.Az:Maritza.Az /home/Maritza.Az/Maritza.Az/files
cat << EOF >> /etc/ssh/sshd_config
Match user Maritza.Az
	ChrootDirectory /home/Maritza.Az/Maritza.Az
	ForceCommand internal-sftp
EOF
/etc/init.d/ssh restart
passwd Maritza.Az
mousepad /etc/ssh/sshd_config
/etc/init.d/ssh restart